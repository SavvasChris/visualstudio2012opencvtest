//Author: Savvas Chris
//Title: OpenCV basics

//Headers
#include <iostream>
#include <string>
#include <windows.h>
#include <opencv2\core\core.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\imgproc\imgproc.hpp>

//function prototypes
void morphological_filters(cv::Mat img);
void color_change(cv::Mat img);
void image_manip(cv::Mat img);

//Main entry point
int main(){

	//Read image from working directory
	//Flag '1' to get color image
	cv::Mat img = cv::imread("lena.png",1);

	//Show image in an OpenCV standard window
	cv::imshow("lena", img);
	std::cout<<"\nClose Window or hit key in Window to continue\n";
	//wait for last window to close
	cv::waitKey(0);

	//Menu
	int choice = 4;

	do{
		system("CLS");
		std::cout<<"OpenCV Basics\nSavvas Chris\n";
		std::string menu_choice = "";
		std::cout<<"\nSelect choices from below:\n1. Basic morphological filters\n"<<
			"2. Access pixels and channels to change values\n3. Other basic operations\n4. Exit\n"<<
			"-> ";
		std::cin>>menu_choice;
		std::cout<<"\n";
		choice = atoi(menu_choice.c_str());
		switch (choice){
			//Basic morphological filters
			case 1: morphological_filters(img); break;
			//Access pixels and channels to change values
			case 2: color_change(img); break;
			//Other basic operations
			case 3: image_manip(img); break;
			case 4: system("CLS"); break;
			default: std::cout<<"Wrong input!\n"; Sleep(1000); 
		}
	}while(choice!=4);
	
}

void morphological_filters(cv::Mat img){

	int choice = 7;
	do{
		system("CLS");
		std::cout<<"OpenCV Basics\nSavvas Chris\n\n";
		std::cout<<"Morphological filters\n";
		std::string menu_choice = "";
		std::cout<<"\nSelect choices from below:\n1. Basic blur\n"<<
			"2. Gaussian blur\n3. Median blur\n4. Bilateral filter\n5. Erode\n6. Dilate\n7. Exit\n"<<
			"-> ";
		std::cin>>menu_choice;
		std::cout<<"\n";
		choice = atoi(menu_choice.c_str());
		switch (choice){
			case 1: {//Blur the image
					cv::Mat blur_img;
					int k_size = 3;
					cv::blur(img, blur_img,cv::Size(k_size,k_size));
					cv::imshow("lena blur", blur_img); 
					std::cout<<"\nClose Window or hit key in Window to continue\n";
					cv::waitKey(0);
					system("CLS");
					break;}
			case 2: {//Gaussian blur the image
					cv::Mat gaussian_blur_img;
					int g_k_size = 3;
					float sigma = 0.5;
					cv::GaussianBlur(img, gaussian_blur_img,cv::Size(g_k_size,g_k_size),sigma);
					cv::imshow("lena gaussian blur", gaussian_blur_img); 
					std::cout<<"\nClose Window or hit key in Window to continue\n";
					cv::waitKey(0);
					system("CLS");
					break;}
			case 3: {//Median blur the image
					cv::Mat median_blur_img;
					int m_k_size = 3;
					cv::medianBlur(img, median_blur_img, m_k_size);
					cv::imshow("lena median blur", median_blur_img);
					std::cout<<"\nClose Window or hit key in Window to continue\n";
					cv::waitKey(0);
					system("CLS");
					break;}
			case 4: {//Bilateral filter the image
					cv::Mat bilateral_filter_img;
					int bf_k_size = 3;
					float c_sigma = 0.5;
					float s_sigma = 0.5;
					cv::bilateralFilter(img, bilateral_filter_img, bf_k_size, c_sigma, s_sigma);
					cv::imshow("lena bilateral filter", bilateral_filter_img);
					std::cout<<"\nClose Window or hit key in Window to continue\n";
					cv::waitKey(0);
					system("CLS");
					break;}
			case 5: {//Erode the image
					cv::Mat erode_img;
					int e_iter = 3;
					cv::erode(img, erode_img, cv::Mat(), cv::Point(-1,-1), e_iter, 1, 1);
					cv::imshow("lena erode", erode_img);
					std::cout<<"\nClose Window or hit key in Window to continue\n";
					cv::waitKey(0);
					system("CLS");
					break;}
			case 6: {//Dilate the image
					cv::Mat dilate_img;
					int d_iter = 3;
					cv::dilate(img, dilate_img, cv::Mat(), cv::Point(-1, -1), d_iter, 1, 1);
					cv::imshow("lena dilate", dilate_img);
					std::cout<<"\nClose Window or hit key in Window to continue\n";
					cv::waitKey(0);
					system("CLS");
					break;}
			case 7: system("CLS"); break;
			default: std::cout<<"Wrong input!\n"; Sleep(1000);
		}
		
	}while(choice!=7);

}

void color_change(cv::Mat img){

	int choice = 3;
	int width = img.cols;
	int height = img.rows;
	//Typical flags: CV_8U CV_8UC1 CV_8UC3 CV_8UC4 CV_32F
	do{
		system("CLS");
		std::cout<<"OpenCV Basics\nSavvas Chris\n\n";
		std::cout<<"Pixel and channel manipulation\n";
		std::string menu_choice = "";
		std::cout<<"\nSelect choices from below:\n1. Grayscale image\n"<<
			"2. Color image\n3. Exit\n"<<
			"-> ";
		std::cin>>menu_choice;
		std::cout<<"\n";
		choice = atoi(menu_choice.c_str());
		switch (choice){
			case 1: {//Iterate through pixels - one channel
						cv::Mat new_float_img = cv::Mat::zeros(height, width, CV_8U);
						cv::Mat new_float_alpha_img = cv::Mat::zeros(height, width, CV_8UC4);
						for(int i=0; i<img.rows; i++){
							for(int j=0; j<img.cols; j++){
								//Access image at pixel level
								if(img.at<cv::Vec3b>(i,j)[0] > 128){
									new_float_img.at<uchar>(i,j) = 255;
									new_float_alpha_img.at<cv::Vec4b>(i,j)[0] = 0;
									new_float_alpha_img.at<cv::Vec4b>(i,j)[1] = 0;
									new_float_alpha_img.at<cv::Vec4b>(i,j)[2] = 0;
									new_float_alpha_img.at<cv::Vec4b>(i,j)[3] = 255;
								}
								else{
									new_float_img.at<uchar>(i,j) = 0;
									new_float_alpha_img.at<cv::Vec4b>(i,j)[0] = 0;
									new_float_alpha_img.at<cv::Vec4b>(i,j)[1] = 0;
									new_float_alpha_img.at<cv::Vec4b>(i,j)[2] = 0;
									new_float_alpha_img.at<cv::Vec4b>(i,j)[3] = 0;
								}
							}
						}
						cv::imshow("new_float_img",new_float_img);
						cv::imwrite("new_float_img.png",new_float_img);
						cv::imshow("new_float_alpha_img",new_float_alpha_img);
						cv::imwrite("new_float_alpha_img.png",new_float_alpha_img);
						std::cout<<"\nClose Window or hit key in Window to continue\n";
						cv::waitKey(0);
						system("CLS");
						break;}
			case 2: {//Iterate through pixels - 3 channels
						cv::Mat new_float_clr_img = cv::Mat::zeros(height, width, CV_8UC3);
						cv::Mat new_float_clr_alpha_img = cv::Mat::zeros(height, width, CV_8UC4);
						for(int i=0; i<img.rows; i++){
							for(int j=0; j<img.cols; j++){
								//Access image at pixel level
								if(img.at<cv::Vec3b>(i,j)[0] > 128){
									new_float_clr_img.at<cv::Vec3b>(i,j)[0] = 255;
									new_float_clr_alpha_img.at<cv::Vec4b>(i,j)[0] = 255;
									new_float_clr_alpha_img.at<cv::Vec4b>(i,j)[3] = 255;
								}
								if(img.at<cv::Vec3b>(i,j)[1] > 128){
									new_float_clr_img.at<cv::Vec3b>(i,j)[1] = 255;
									new_float_clr_alpha_img.at<cv::Vec4b>(i,j)[1] = 255;
									new_float_clr_alpha_img.at<cv::Vec4b>(i,j)[3] = 200;
								}
								if(img.at<cv::Vec3b>(i,j)[2] > 128){
									new_float_clr_img.at<cv::Vec3b>(i,j)[2] = 255;
									new_float_clr_alpha_img.at<cv::Vec4b>(i,j)[2] = 255;
									new_float_clr_alpha_img.at<cv::Vec4b>(i,j)[3] = 100;
								}
								else{
									new_float_clr_alpha_img.at<cv::Vec4b>(i,j)[0] = 0;
									new_float_clr_alpha_img.at<cv::Vec4b>(i,j)[1] = 0;
									new_float_clr_alpha_img.at<cv::Vec4b>(i,j)[2] = 0;
									new_float_clr_alpha_img.at<cv::Vec4b>(i,j)[3] = 0;
								}
							}
						}
						cv::imshow("new_float_clr_img",new_float_clr_img);
						cv::imshow("new_float_clr_alpha_img",new_float_clr_alpha_img);
						cv::imwrite("new_float_clr_img.png",new_float_clr_img);
						cv::imwrite("new_float_clr_alpha_img.png",new_float_clr_alpha_img);
						std::cout<<"\nClose Window or hit key in Window to continue\n";
						cv::waitKey(0);
						system("CLS");
						break;}
					case 3: system("CLS"); break;
			default: std::cout<<"Wrong input!\n"; Sleep(1000);
		}
	}while(choice!=3);
	
}

void image_manip(cv::Mat img){

	int choice = 5;
	do{
		system("CLS");
		std::cout<<"OpenCV Basics\nSavvas Chris\n\n";
		std::cout<<"Image manipulation\n";
		std::string menu_choice = "";
		std::cout<<"\nSelect choices from below:\n1. Resize\n"<<
			"2. Change color space\n3. Canny edge detector\n4. Sobel operator\n5. Exit\n"<<
			"-> ";
		std::cin>>menu_choice;
		std::cout<<"\n";
		choice = atoi(menu_choice.c_str());
		switch (choice){
			case 1:{//Resize image
					cv::Mat small_img;
					cv::Mat big_img;
					cv::resize(img,small_img,cv::Size((int)img.rows/2,(int)img.cols/2));
					cv::resize(img,big_img,cv::Size((int)img.rows*2,(int)img.cols*2));
					cv::imshow("big_img",big_img);
					cv::imwrite("big_img.png",big_img);
					cv::imshow("small_img",small_img);
					cv::imwrite("small_img.png",small_img);
					std::cout<<"\nClose Window or hit key in Window to continue\n";
					cv::waitKey(0);
					system("CLS");
				    break;}
			case 2:{//Change color space
					cv::Mat gray_img;
					cv::Mat hsv_img;
					cv::Mat ycrcb_img;
					cv::cvtColor(img, gray_img, CV_BGR2GRAY);
					cv::cvtColor(img, hsv_img, CV_BGR2HSV);
					cv::cvtColor(img, ycrcb_img, CV_BGR2YCrCb);
					cv::imshow("gray_img",gray_img);
					cv::imwrite("gray_img.png",gray_img);
					cv::imshow("hsv_img",hsv_img);
					cv::imwrite("hsv_img.png",hsv_img);
					cv::imshow("ycrcb_img",ycrcb_img);
					cv::imwrite("ycrcb_img.png",ycrcb_img);
					std::cout<<"\nClose Window or hit key in Window to continue\n";
					cv::waitKey(0);
					system("CLS");
					break;}
			case 3:{//Canny edge detector
					cv::Mat canny_img;
					int thresh1 = 100, thresh2 = 100, aperture = 3;
					cv::Canny(img, canny_img, thresh1, thresh2, aperture);
					cv::imshow("canny_img",canny_img);
					cv::imwrite("canny_img.png",canny_img);
					std::cout<<"\nClose Window or hit key in Window to continue\n";
					cv::waitKey(0);
					system("CLS");
				    break;}
			case 4:{//Sobel operator
					cv::Mat gray_img, grad_x, grad_y, abs_grad_x, abs_grad_y, grad;
					int ddepth = CV_16S;
					cv::cvtColor(img, gray_img, CV_BGR2GRAY);
					cv::Sobel(gray_img, grad_x, ddepth, 1, 0, 3, 1, 0, cv::BORDER_DEFAULT);
					cv::Sobel(gray_img, grad_y, ddepth, 0, 1, 3, 1, 0, cv::BORDER_DEFAULT);
					convertScaleAbs(grad_x, abs_grad_x);
					convertScaleAbs(grad_y, abs_grad_y);
					addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad);
					cv::imshow("grad", grad);
					cv::imwrite("grad.png",grad);
					std::cout<<"\nClose Window or hit key in Window to continue\n";
					cv::waitKey(0);
					system("CLS");
				    break;}
			case 5: system("CLS"); break;
			default: std::cout<<"Wrong input!\n"; Sleep(1000);
		}
	}while(choice!=5);
	
}